+++
title = "PGallery"
description = "A dead simple photo gallery made for Pinephone. Built with Vala and Gtk"
aliases = []
date = 2020-12-12
updated = 2023-09-30

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "joancipria",]
categories = [ "image viewer",]
mobile_compatibility = [ "5",]
status = [ "archived",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Graphics", "Viewer",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/joancipria/pgallery"
homepage = ""
bugtracker = "https://github.com/joancipria/pgallery/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/joancipria/pgallery"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "com.github.joancipria.pgallery"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "linmob"

+++

### Notice

Archived on Oct 4, 2021.
