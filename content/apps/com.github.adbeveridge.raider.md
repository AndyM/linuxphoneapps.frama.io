+++
title = "File Shredder"
description = "Securely delete your files"
aliases = []
date = 2022-09-17
updated = 2023-09-30

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Alan Beveridge",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "shred",]
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/ADBeveridge/raider"
homepage = "https://github.com/ADBeveridge/raider"
bugtracker = "https://github.com/ADBeveridge/raider/issues/"
donations = ""
translations = "https://github.com/ADBeveridge/raider/tree/develop/po"
more_information = [ "https://apps.gnome.org/app/com.github.ADBeveridge.Raider/",]
summary_source_url = "https://github.com/ADBeveridge/raider"
screenshots = [ "https://raw.githubusercontent.com/ADBeveridge/raider/main/data/raider-screenshot1.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/com.github.adbeveridge.raider/1.png", "https://img.linuxphoneapps.org/com.github.adbeveridge.raider/2.png", "https://img.linuxphoneapps.org/com.github.adbeveridge.raider/3.png", "https://img.linuxphoneapps.org/com.github.adbeveridge.raider/4.png", "https://img.linuxphoneapps.org/com.github.adbeveridge.raider/5.png", "https://img.linuxphoneapps.org/com.github.adbeveridge.raider/6.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "com.github.ADBeveridge.Raider"
scale_to_fit = "com.github.ADBeveridge.Raider.Help"
flathub = "https://flathub.org/apps/com.github.ADBeveridge.Raider"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "raider-file-shredder",]
appstream_xml_url = "https://raw.githubusercontent.com/ADBeveridge/raider/develop/data/com.github.ADBeveridge.Raider.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++

### Description
File Shredder is a GNOME application used for securely deleting your files
 that you do not want to be recovered.

[Source](https://raw.githubusercontent.com/ADBeveridge/raider/develop/data/com.github.ADBeveridge.Raider.metainfo.xml)
