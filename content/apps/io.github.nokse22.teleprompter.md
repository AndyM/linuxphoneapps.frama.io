+++
title = "Teleprompter"
description = "Display scrolling text on your screen"
aliases = []
date = 2023-06-25
updated = 2023-09-30

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Nokse",]
categories = [ "teleprompter",]
mobile_compatibility = [ "4",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Utility", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/Nokse22/teleprompter"
homepage = "https://github.com/Nokse22/teleprompter"
bugtracker = "https://github.com/Nokse22/teleprompter/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = ""
screenshots = [ "https://raw.githubusercontent.com/Nokse22/teleprompter/main/data/resources/Screenshot%201.png", "https://raw.githubusercontent.com/Nokse22/teleprompter/main/data/resources/Screenshot%202.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/io.github.nokse22.teleprompter/1.png", "https://img.linuxphoneapps.org/io.github.nokse22.teleprompter/2.png",]
all_features_touch = true
intended_for_mobile = false
app_id = "io.github.nokse22.teleprompter"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.nokse22.teleprompter"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/Nokse22/teleprompter/main/data/io.github.nokse22.teleprompter.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++

### Description
A simple app to read scrolling text from your screen. It has an adaptive layout to work on smartphones too.

[Source](https://raw.githubusercontent.com/Nokse22/teleprompter/main/data/io.github.nokse22.teleprompter.appdata.xml.in)

### Notice 
Just a few pixels too wide in portrait orientation.
