+++
title = "KDE Connect"
description = "Seamless connection of your devices"
aliases = []
date = 2019-02-01
updated = 2023-09-30

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "connectivity",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = [ "KDE Connect",]
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_stable_23_05", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Network", "Network",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/network/kdeconnect-kde"
homepage = "https://kdeconnect.kde.org"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?format=guided&product=kdeconnect"
donations = ""
translations = ""
more_information = [ "https://invent.kde.org/network/kdeconnect-android",]
summary_source_url = "https://invent.kde.org/network/kdeconnect-kde/-/raw/master/data/org.kde.kdeconnect.metainfo.xml"
screenshots = [ "https://cdn.kde.org/screenshots/kdeconnect/plasmoid.png", "https://cdn.kde.org/screenshots/kdeconnect/kcm.png", "https://cdn.kde.org/screenshots/kdeconnect/gnome3.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.kdeconnect"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kdeconnect",]
appstream_xml_url = "https://invent.kde.org/network/kdeconnect-kde/-/raw/master/data/org.kde.kdeconnect.metainfo.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++

### Description
KDE Connect provides various features to integrate your phone and your computer. It allows you to send files to the other device, control its media playback, send remote input, view its notifications and may things more. It is available for (mobile) Linux, Android, FreeBSD, Windows and macOS.

[Source](https://invent.kde.org/network/kdeconnect-kde/-/raw/master/data/org.kde.kdeconnect.metainfo.xml)
