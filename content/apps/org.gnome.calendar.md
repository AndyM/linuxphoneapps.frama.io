+++
title = "Calendar"
description = "Calendar for GNOME"
aliases = []
date = 2022-09-28
updated = 2023-09-30

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "GNOME Developers",]
categories = [ "calendar",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_stable_23_05", "nix_unstable", "opensuse_tumbleweed", "pureos_landing", "snapcraft",]
freedesktop_categories = [ "GTK", "GNOME", "Office", "Calendar",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-calendar"
homepage = "https://wiki.gnome.org/Apps/Calendar"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-calendar/issues"
donations = "https://www.gnome.org/donate/"
translations = ""
more_information = [ "https://apps.gnome.org/app/org.gnome.Calendar/",]
summary_source_url = "https://gitlab.gnome.org/GNOME/gnome-calendar/-/raw/main/data/appdata/org.gnome.Calendar.appdata.xml.in.in"
screenshots = [ "https://static.gnome.org/appdata/gnome-43/calendar/calendar-week.png", "https://static.gnome.org/appdata/gnome-43/calendar/calendar-month.png", "https://static.gnome.org/appdata/gnome-43/calendar/calendar-event.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/org.gnome.calendar/1.png", "https://img.linuxphoneapps.org/org.gnome.calendar/2.png", "https://img.linuxphoneapps.org/org.gnome.calendar/3.png", "https://img.linuxphoneapps.org/org.gnome.calendar/4.png", "https://img.linuxphoneapps.org/org.gnome.calendar/5.png",]
all_features_touch = true
intended_for_mobile = false
app_id = "org.gnome.Calendar"
scale_to_fit = "org.gnome.Calendar"
flathub = "https://flathub.org/apps/org.gnome.Calendar"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = "https://snapcraft.io/gnome-calendar"
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-calendar",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-calendar/-/raw/main/data/appdata/org.gnome.Calendar.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "linmob"

+++

### Description
GNOME Calendar is a simple and beautiful calendar application designed to perfectly fit the GNOME
 desktop. By reusing the components which the GNOME desktop is built on, Calendar nicely integrates
 with the GNOME ecosystem.

We aim to find the perfect balance between nicely crafted features and user-centred usability. No
 excess, nothing missing. You’ll feel comfortable using Calendar, like you’ve been using it for ages!

[Source](https://gitlab.gnome.org/GNOME/gnome-calendar/-/raw/main/data/appdata/org.gnome.Calendar.appdata.xml.in.in)

### Notice
With release 43, this app has been redesigned to be mobile friendly. In initial testing (flathub), it misses this goal by a few pixels (see first screenshot). Previously, Purism had created an adaptive fork of GNOME Calendar 41 for the Librem 5. With GNOME 45, it fits the phone screen without issues.
