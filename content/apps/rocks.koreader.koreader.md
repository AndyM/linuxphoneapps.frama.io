+++
title = "KOReader"
description = "A document viewer for DjVu, PDF, EPUB and more"
aliases = []
date = 2023-11-21

[taxonomies]
project_licenses = [ "AGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "NiLuJe",]
categories = [ "document viewer", "pdf viewer", "feed reader",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "SDL",]
backends = [ "mupdf",]
services = [ "RSS",]
packaged_in = [ "aur", "flathub", "nix_stable_21_11", "nix_stable_22_05", "nix_stable_22_11", "nix_stable_23_05", "nix_unstable" ]
freedesktop_categories = [ "Graphics",]
programming_languages = [ "Lua",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/koreader/koreader"
homepage = "https://koreader.rocks/"
bugtracker = "https://github.com/koreader/koreader/issues"
donations = ""
translations = "https://github.com/koreader/koreader-translations"
more_information = [ "https://github.com/koreader/koreader/wiki",]
summary_source_url = "https://raw.githubusercontent.com/koreader/koreader/master/platform/appimage/koreader.appdata.xml"
screenshots = [ "https://github.com/koreader/koreader-artwork/raw/master/koreader-menu-thumbnail.png", "https://github.com/koreader/koreader-artwork/raw/master/koreader-footnotes-thumbnail.png", "https://github.com/koreader/koreader-artwork/raw/master/koreader-dictionary-thumbnail.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = false
app_id = "rocks.koreader.KOReader"
scale_to_fit = ""
flathub = "https://flathub.org/apps/rocks.koreader.KOReader"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "koreader",]
appstream_xml_url = "https://raw.githubusercontent.com/koreader/koreader/master/platform/appimage/koreader.appdata.xml"
reported_by = "colinsane"
updated_by = ""

+++


### Description
KOReader is a document viewer primarily aimed at e-ink readers.

Main features

* **portable**: runs on embedded devices (Cervantes, Kindle, Kobo, PocketBook, reMarkable), Android and Linux computers. Developers can run a KOReader emulator in Linux and MacOS.

* **multi-format documents**: supports fixed page formats (PDF, DjVu, CBT, CBZ) and reflowable e-book formats (EPUB, FB2, Mobi, DOC, RTF, HTML, CHM, TXT). Scanned PDF/DjVu documents can also be reflowed with the built-in K2pdfopt library. [ZIP files][link-wiki-zip] are also supported for some formats.

* **full-featured reading**: multi-lingual user interface with a highly customizable reader view and many typesetting options. You can set arbitrary page margins, override line spacing and choose external fonts and styles. It has multi-lingual hyphenation dictionaries bundled into the application.

* **integrated** with *calibre* (search metadata, receive ebooks wirelessly, browse library via OPDS), *Wallabag*, *Wikipedia*, *Google Translate* and other content providers.

* **optimized for e-ink devices**: custom UI without animation, with paginated menus, adjustable text contrast, and easy zoom to fit content or page in paged media.

* **extensible**: via plugins

* **fast**: on some older devices, it has been measured to have less than half the page-turn delay as the built in reading software.

* **and much more**: look up words with StarDict dictionaries / Wikipedia, add your own online OPDS catalogs and RSS feeds, over-the-air software updates, an FTP client, an SSH server, …

[Source](https://raw.githubusercontent.com/koreader/koreader/master/README.md)

