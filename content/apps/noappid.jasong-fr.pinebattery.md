+++
title = "PineBattery"
description = "GTK app for monitoring the PinePhone and PineTab battery."
aliases = []
date = 2021-04-24
updated = 2023-09-30

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "jasong-fr",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "inactive",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utiltiy",]
programming_languages = [ "Python",]
build_systems = [ "custom",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/JasonG-FR/PineBattery"
homepage = ""
bugtracker = "https://github.com/JasonG-FR/PineBattery/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/JasonG-FR/PineBattery"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++

### Notice
Cloning the repo and running install.sh is enough to try this simple battery monitor.
