+++
title = "Daty"
description = "Free Wikidata editor."
aliases = []
date = 2019-02-16
updated = 2023-09-30

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Pellegrino Prevete",]
categories = [ "utilities",]
mobile_compatibility = [ "4",]
status = [ "inactive",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "Wikidata",]
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/World/Daty"
homepage = "https://gitlab.gnome.org/World/Daty/"
bugtracker = "https://gitlab.gnome.org/World/Daty/issues"
donations = "http://patreon.com/tallero"
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/World/Daty/-/raw/master/flatpak/ml.prevete.Daty.appdata.xml"
screenshots = [ "https://gitlab.gnome.org/World/Daty/raw/master/screenshots/editor.png", "https://gitlab.gnome.org/World/Daty/raw/master/screenshots/mobile-view-page.png", "https://gitlab.gnome.org/World/Daty/raw/master/screenshots/mobile-view-sidebar.png", "https://gitlab.gnome.org/World/Daty/raw/master/screenshots/free-text-search.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "ml.prevete.Daty"
scale_to_fit = "daty"
flathub = "https://flathub.org/apps/ml.prevete.Daty"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "daty",]
appstream_xml_url = "https://gitlab.gnome.org/World/Daty/-/raw/master/flatpak/ml.prevete.Daty.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++

### Description
Daty is a free Wikidata editor adhering to GNOME Human Interface Guidelines, intended to enable a better editing workflow and faster deployment of requested user features for Wikidata.

Use Daty to search, select, read, batch edit items, script actions, share, visualize proposed changes and bots.

ATTENTION: This is a preview release, so entities editing is disabled.

[Source](https://gitlab.gnome.org/World/Daty/-/raw/master/flatpak/ml.prevete.Daty.appdata.xml)

### Notice
Inactive since December 2020.
