+++
title = "mobpass"
description = "A mobile interface for gopass"
aliases = []
date = 2020-03-24
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "anjan",]
categories = [ "password manager",]
mobile_compatibility = [ "5",]
status = [ "early", "inactive",]
frameworks = [ "QtQuick",]
backends = [ "gopass",]
services = []
packaged_in = [ "alpine_edge",]
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "none",]
requires_internet = []
tags = []

[extra]
repository = "https://git.sr.ht/~anjan/mobpass"
homepage = ""
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://git.sr.ht/~anjan/mobpass"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "mobpass",]
appstream_xml_url = ""
reported_by = "anjandev"
updated_by = "linmob"

+++

### Notice

Currently broken in Alpine Edge due to hardcoded paths for Python 3.8 with later Python releases being current.
