+++
title = "loyaltyCardsOpen"
description = "Linux App to save and view all your loyalty cards and any kind of card. Ready to use with a Linux Phone."
aliases = []
date = 2020-12-19
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "joanisc",]
categories = [ "loyalty cards",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "setup.py",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/joanisc/loyaltyCardsOpen"
homepage = ""
bugtracker = "https://github.com/joanisc/loyaltyCardsOpen/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/joanisc/loyaltyCardsOpen"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++
