+++
title = "Kalm"
description = "Kalm can teach you different breathing techniques."
aliases = []
date = 2023-10-21

[taxonomies]
project_licenses = [ "LGPL-2.1-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "plata",]
categories = [ "education", "health"]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Utility", "KDE", "Qt",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/utilities/kalm"
homepage = "https://apps.kde.org/kalm"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=Kalm"
donations = "https://kde.org/community/donations"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/utilities/kalm/-/raw/master/org.kde.kalm.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/kalm/general.png", "https://cdn.kde.org/screenshots/kalm/info.png", "https://cdn.kde.org/screenshots/kalm/menu.png",]
screenshots_img = []
all_features_touch = false 
intended_for_mobile = false
app_id = "org.kde.kalm"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kalm",]
appstream_xml_url = "https://invent.kde.org/utilities/kalm/-/raw/master/org.kde.kalm.appdata.xml"
reported_by = "plata"
+++

### Description
Kalm can teach you different breathing techniques.

Currently supported techniques:
- 4-7-8 Breathing
- Coordinated Breathing
- Box Breathing
- Box Breathing (sleep)
- Resonant Breathing
- Nadi Shodhana
- Yogic Breathing
