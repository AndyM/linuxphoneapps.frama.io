+++
title = "Wince"
description = "Search for restaurants and businesses"
aliases = []
date = 2022-12-25

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = [ "wildeyedskies",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Yelp",]
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Crystal",]
build_systems = [ "make",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/wildeyedskies/wince"
homepage = ""
bugtracker = "https://gitlab.gnome.org/wildeyedskies/wince/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/wildeyedskies/wince"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "space.quietfeathers.Wince"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "wildeyedskies"
updated_by = ""

+++

### Description
