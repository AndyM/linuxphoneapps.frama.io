+++
title = "Phonebook"
description = "View and edit contacts"
aliases = []
date = 2019-09-30
updated = 2023-09-30

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "contacts",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = [ "kpeople", "kpeoplevcard", "Kcontacts",]
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "aur", "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_stable_23_05", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Office", "ContactManagement",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/plasma-mobile/plasma-phonebook"
homepage = ""
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=Plasma%20Mobile%20Phonebook"
donations = ""
translations = ""
more_information = [ "https://phabricator.kde.org/T6937",]
summary_source_url = "https://invent.kde.org/plasma-mobile/plasma-phonebook/-/raw/master/org.kde.phonebook.metainfo.xml"
screenshots = [ "https://cdn.kde.org/screenshots/plasma-phonebook/plasma-phonebook.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.phonebook"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "plasma-phonebook",]
appstream_xml_url = "https://invent.kde.org/plasma-mobile/plasma-phonebook/-/raw/master/org.kde.phonebook.metainfo.xml"
reported_by = "nicolasfella"
updated_by = "script"

+++

### Description
Convergent contact management application for both desktop and mobile devices.

It provides a central place for starting conversations. Depending on the information available about a contact,
 respective actions are displayed.

Features

* Add contacts
* Remove contacts
* Import vCard files

[Source](https://invent.kde.org/plasma-mobile/plasma-phonebook/-/raw/master/org.kde.phonebook.metainfo.xml)
